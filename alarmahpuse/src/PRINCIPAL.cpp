#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <FirebaseESP8266.h>
//-------------------VARIABLES GLOBALES--------------------------
int contconexion = 0;
//-------------------VARIABLES MI CODIGO-----------------------
 
#define PULSADOR 5
#define sensorPin 13        //pin que va conectado al sensor
#define alarmaPin 12   //pin que va conectado al buzzer y a los leds
#define FIREBASE_HOST "tpmoroni-ab269-default-rtdb.firebaseio.com" //host de firebase
#define FIREBASE_AUTH "rpvXcFbmpT4ME3AtGvs77B8uzbun4PZj0Vyz1Qgz" // autenticador de firebase
FirebaseData TPmoroni;
String ruta = "prenderalarma/Sensor" ;
String rutaEstado = "/prenderalarma/Estado";
bool lectura = 0;
bool prenderalarma = false;

void setup_wifi();
void paginaconf();
void modoconf();
void guardar_conf();
void grabar(int addr, String a);
String leer(int addr);
void escanear();

char ssid[50];      
char pass[50];

const char *ssidConf = "fefeagus";
const char *passConf = "12345678";

String mensaje = "";

//-----------CODIGO HTML PAGINA DE CONFIGURACION---------------
String pagina = "<!DOCTYPE html>"
"<html>"
"<head>"
"<title>eprom fefeagus</title>"
"<meta charset='UTF-8'>"
"</head>"
"<body>"
"</form>"
"<form action='guardar_conf' method='get'>"
"SSID:<br><br>"
"<input class='input1' name='ssid' type='text'><br>"
"PASSWORD:<br><br>"
"<input class='input1' name='pass' type='password'><br><br>"
"<input class='boton' type='submit' value='GUARDAR'/><br><br>"
"</form>"
"<a href='escanear'><button class='boton'>ESCANEAR</button></a><br><br>";

String paginafin = "</body>"
"</html>";

//------------------------SETUP WIFI-----------------------------
void setup_wifi() {
// Conexión WIFI
  WiFi.mode(WIFI_STA); //para que no inicie el SoftAP en el modo normal
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED and contconexion <50) { //Cuenta hasta 50 si no se puede conectar lo cancela
    ++contconexion;
    delay(250);
    Serial.print(".");
    
  }
  if (contconexion <50) {   
      Serial.println("");
      Serial.println("WiFi conectado");
      Serial.println(WiFi.localIP());
      digitalWrite(13, HIGH);  
  }
  else { 
      Serial.println("");
      Serial.println("Error de conexion");
      digitalWrite(13, LOW);
  }
    
}

//--------------------------------------------------------------
WiFiClient espClient;
ESP8266WebServer server(80);
//--------------------------------------------------------------

//-------------------PAGINA DE CONFIGURACION--------------------
void paginaconf() {
  server.send(200, "text/html", pagina + mensaje + paginafin); 
}

//--------------------MODO_CONFIGURACION------------------------
void modoconf() {

  WiFi.softAP(ssidConf, passConf);
  IPAddress myIP = WiFi.softAPIP(); 
  Serial.print("IP del acces point: ");
  Serial.println(myIP);
  Serial.println("WebServer iniciado...");

  server.on("/", paginaconf); //esta es la pagina de configuracion

  server.on("/guardar_conf", guardar_conf); //Graba en la eeprom la configuracion

  server.on("/escanear", escanear); //Escanean las redes wifi disponibles
  
  server.begin();

    
    Firebase.reconnectWiFi(true);

  while (true) {
      server.handleClient();
  }
}

//---------------------GUARDAR CONFIGURACION-------------------------
void guardar_conf() {
  
  Serial.println(server.arg("ssid"));//Recibimos los valores que envia por GET el formulario web
  grabar(0,server.arg("ssid"));
  Serial.println(server.arg("pass"));
  grabar(50,server.arg("pass"));

  mensaje = "Configuracion Guardada...";
  paginaconf();
}

//----------------Función para grabar en la EEPROM-------------------
void grabar(int addr, String a) {
  int tamano = a.length(); 
  char inchar[50]; 
  a.toCharArray(inchar, tamano+1);
  for (int i = 0; i < tamano; i++) {
    EEPROM.write(addr+i, inchar[i]);
  }
  for (int i = tamano; i < 50; i++) {
    EEPROM.write(addr+i, 255);
  }
  EEPROM.commit();
}

//-----------------Función para leer la EEPROM------------------------
String leer(int addr) {
   byte lectura;
   String strlectura;
   for (int i = addr; i < addr+50; i++) {
      lectura = EEPROM.read(i);
      if (lectura != 255) {
        strlectura += (char)lectura;
      }
   }
   return strlectura;
}

//---------------------------ESCANEAR----------------------------
void escanear() {  
  int n = WiFi.scanNetworks(); //devuelve el número de redes encontradas
  Serial.println("escaneo terminado");
  if (n == 0) { //si no encuentra ninguna red
    Serial.println("no se encontraron redes");
    mensaje = "no se encontraron redes";
  }  
  else
  {
    Serial.print(n);
    Serial.println(" redes encontradas");
    mensaje = "";
    for (int i = 0; i < n; ++i)
    {
    
      mensaje = (mensaje) + "<p>" + String(i + 1) + ": " + WiFi.SSID(i) + " (" + WiFi.RSSI(i) + ") Ch: " + WiFi.channel(i) + " Enc: " + WiFi.encryptionType(i) + " </p>\r\n";
     
      delay(10);
    }
    Serial.println(mensaje);
    paginaconf();
  }
}

//------------------------SETUP-----------------------------
void setup() { 
    pinMode(sensorPin, INPUT);
    pinMode(alarmaPin, OUTPUT);
    pinMode(PULSADOR, INPUT);

      // Inicia Serial
  EEPROM.begin(512);
  Serial.begin(9600);
  Serial.println("");

  if (digitalRead(PULSADOR) == 1) {
    modoconf();
  }

  leer(0).toCharArray(ssid, 50);
  leer(50).toCharArray(pass, 50);
  setup_wifi();
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

//--------------------------LOOP--------------------------------
void loop() {
  
    //digitalWrite(alarmaPin, HIGH);
    //Serial.println(Firebase.getBool(TPmoroni, "/prenderalarma/Estado"));
    Firebase.getBool(TPmoroni, "/prenderalarma/Estado"); //esto alamacena la data en el atribu
    if( TPmoroni.boolData() == 1){  
  
      lectura = digitalRead(sensorPin); //el sensor comienza a funcionar

      Serial.println("prendido");
      delay(1000);
      Firebase.setBool(TPmoroni, "/prenderalarma/Sensor", lectura);
      Serial.println(lectura);
      if(lectura == 0){
        digitalWrite(alarmaPin, HIGH);
      }else{
        digitalWrite(alarmaPin, LOW);     
      }
    }

    else { //si estado alarma es 0 (o sea el boton de la web esta apagado)
     Serial.println("apagado"); //y la consola printea apagado
    
     delay(1000);
    
  }
}