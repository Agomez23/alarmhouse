import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getDatabase, ref, set, push, onValue, orderByKey, query  } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js';
const firebaseConfig = {
	apiKey: "AIzaSyC-aIVlcx_JutFB0dLLwWCskynddFR9SEI",
	authDomain: "tpmoroni-ab269.firebaseapp.com",
	databaseURL: "https://tpmoroni-ab269-default-rtdb.firebaseio.com",
	projectId: "tpmoroni-ab269",
	storageBucket: "tpmoroni-ab269.appspot.com",
	messagingSenderId: "170765945889",
	appId: "1:170765945889:web:ff48da51d6eabf35e2a760"
  };
  
  // inicio de firebase
  	const app = initializeApp(firebaseConfig);
    const database = getDatabase(app);
  	const auth = getAuth();
	const user = auth.currentUser;

	var correo;
	var UID;

	let botonOnRef = document.getElementById("ON")
	let botonOffRef = document.getElementById("OFF")
	let InfraRef = document.getElementById("Infra");
	let PrendidoRef = document.getElementById("Prendido")
  	let navRef = document.getElementById("navId")
  	onAuthStateChanged(auth, (user) => {

  		if (user) {
			const uid = user.uid;
			UID = uid;

			const email = user.email;
			correo = email
			botonOnRef.addEventListener("click", EnviarTrue)
			botonOffRef.addEventListener("click", EnviarFalse)
			navRef.innerHTML = `
			<button id="logoutId" type="button" class="btn btn-success w-100 mb-4">Logout</button>
			`
			let logoutRef = document.getElementById("logoutId")
			logoutRef.addEventListener("click", Logout)
		}
		else{
			botonOffRef.addEventListener("click", Ventana);
			botonOnRef.addEventListener("click", Ventana);
		}
	});
	function Logout(){
	signOut(auth).then(() => {
		// Sign-out successful.
		location.reload()
		console.log("Hola")
	  }).catch((error) => {
		// An error happened.
		console.log("Hola2")
	  });
}
	function Ventana(){
		alert("Inicie Sesion para continuar")
	}

	function EnviarTrue(){
		set(ref(database, "prenderalarma/"), {
		  Estado: true,
	  
	  })
	  }
	
	  function EnviarFalse(){
		set(ref(database, "prenderalarma/"), {
		  Estado: false,
	  
	  })
	  }
	

	  const prenderalarmaRef = ref(database, '/prenderalarma/Estado');
	  onValue(prenderalarmaRef, (snapshot) => {
		const dataAlarma = snapshot.val();
		if (dataAlarma == true){
		  var dataAlarma1 = "Estado: Encendido"
		  const RutaRef = ref(database, '/prenderalarma/Sensor/');
	onValue(RutaRef, (snapshot) => {
  	const data = snapshot.val();

	if(data == 0){
		InfraRef.innerHTML = `
		<p style="font-size: 40px; color: #616467;">DETECTANDO ALGO</p>
		`;
	}

	else{
    InfraRef.innerHTML = `
  <p style="font-size: 50px; color: #616467;">Nada se detecta</p>
  `;
	}
})
		  
		}
		else{
		  var dataAlarma1 = "Estado: Apagado"
		  InfraRef.innerHTML = `
		  <p style="font-size: 50px; color: #616467;">Nada se detecta</p>
		  `;
		}
		// AlarmaRef.innerHTML = ""
		PrendidoRef.innerHTML = `<p style = " font-size : 13px; color: #616467;" >${dataAlarma1}</p>`;
	})
	
